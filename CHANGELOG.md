#### 1.2.2 (2022-04-14)

#### 1.2.1 (2022-04-14)

#### 1.2.0 (2021-12-28)

##### Chores

*  bump dependencies + remove twitter (41bf3344)
*  update dependencies (8a497ec7)

#### 1.1.4 (2020-08-04)

##### Chores

*  update dependencies + fix typo (cff345ee)
*  update dependencies (0e7a4451)
*  update dependencies (c122cf3d)
*  update dependencies (78bcdbbd)
*  update dependencies + config (879995b0)
*  update dependencies (7a530e5c)
*  update dependencies (ed1450f3)
*  update dependencies (7ca58cc9)
*  update dependencies (42a478db)
*  move configuration to package.json (a3af48a9)

##### New Features

*  update CV (ec40d6be)
* **app-bar:**  add version number (61009e37)

##### Bug Fixes

*  update profession (19fc3f8f)
* **bottom-nav:**  button size (8506e1dd)

#### 1.1.3 (2020-06-01)

##### Chores

*  update dependencies (c122cf3d)
*  update dependencies (78bcdbbd)
*  update dependencies + config (879995b0)
*  update dependencies (7a530e5c)
*  update dependencies (ed1450f3)

##### New Features

*  update CV (ec40d6be)
* **app-bar:**  add version number (61009e37)

##### Bug Fixes

*  update profession (19fc3f8f)

#### 1.1.2 (2020-01-05)

##### Chores

*  update dependencies (7ca58cc9)
*  update dependencies (42a478db)
*  move configuration to package.json (a3af48a9)

##### Bug Fixes

* **bottom-nav:**  button size (8506e1dd)

#### 1.1.1 (2019-08-19)

##### New Features

*  generate changelog on version change (178f45cf)

#### 1.1.0 (2019-08-19)

##### Chores

*  update packages (7b85d3a3)
*  update packages (0bbd05e9)

##### Continuous Integration

*  switch to npm ci (6667c1f3)

##### New Features

*  add serviceworker support (88614114)
*  switch to Material Design icons (7a786c77)
*  add ripple effect to social links (cf3af883)

##### Bug Fixes

*  bottom nav icons (9e3b4ad8)
*  add gulp script to change sw paths (a97a7e9c)
*  subdirectory for service worker (15bdbcbb)
*  add correct favicon sizes (ba80c1e8)

##### Refactors

*  update to vuetify 2.0.7 + CV cleanup (6a4b2db2)

#### 1.0.0 (2019-06-12)

##### Breaking Changes

*  only support evergreen browsers (98d5ba50)

##### Chores

*  change npm task alias serve to start (44618b83)
*  update packages (c0e6348d)
*  update packages + minor styling fixes (3c3399bc)
*  cleanup gulp file (3fbfd491)
*  update packages + remove vuex, vue-analytics (4625b7f1)

##### Continuous Integration

*  cache node_modules between stages (b74ad906)

##### New Features

*  add projects page + styling fixes (617be597)
*  add not found page (6fd8af77)

##### Bug Fixes

*  minor styling enhancements (6867b8ae)

##### Other Changes

*  update CV page (0d415d19)

