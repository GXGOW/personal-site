export type Project = {
  title: string;
  image: string;
  description: string;
  links: {
    preview?: string;
    git: string;
  };
};
