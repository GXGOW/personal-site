export type DescriptionItem = {
  title: string;
  value: string;
};
