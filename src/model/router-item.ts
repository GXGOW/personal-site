export type RouterItem = {
  title: string;
  route: string;
  icon: string;
};
