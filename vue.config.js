const manifestJson = require('./public/manifest.json');

module.exports = {
  pwa: {
    themeColor: manifestJson.theme_color,
    name: manifestJson.short_name,
    workboxOptions: {
      skipWaiting: true,
    },
  },
};
